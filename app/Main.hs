module Main where

import Control.Monad.IO.Class (liftIO)
import Network.HTTP.Types.Status (ok200)
import System.Random
import Web.Scotty (ActionM, scotty, get, post, status, json, jsonData)

import BattleSnake

main :: IO ()
main = do
  putStrLn "Starting Server"
  scotty 3000 $ do
    get  ""       $ json getInfo
    get  "/start"   ok
    get  "/end"     ok
    get  "/move"  $ handleRequest  -- Not needed except for easier debugging
    post "/move"  $ handleRequest
  where ok = status ok200

-- By default print message based on request, calculates move and prints JSON
handleRequest :: ActionM ()
handleRequest = do
  req <- jsonData
  g <- getStdGen
  liftIO $ putStrLn $ logRequest req
  let next = move g req
  liftIO $ putStrLn $ "Going to reply with " <> show next
  json $ next
