module BattleSnake where

import Data.Aeson (FromJSON, Value, encode, decode, object, (.=))
import Data.Aeson.Schema (Object, get)
import Data.List (sortOn)
import Data.Maybe (fromJust, mapMaybe)
import Data.Text (Text)
import Debug.Trace
import System.Random

import Data.Map (Map)
import qualified Data.Map as M

-- JSON Schemas for use with [get| ... |]
import Schema.Coord (Coord)
import Schema.Info (Info)
import Schema.Move (Move)
import Schema.Request (Request)
import Schema.Snake (Snake)

type Coordinate = (Int, Int)

-- Source: https://www.e-learn.cn/topic/3465929
coerceJson :: FromJSON a => Value -> a
coerceJson = fromJust . decode . encode

-- Change this to customise your snek
getInfo :: Object Info
getInfo = coerceJson $ object [ "apiversion" .= ("1"                  :: Text)
                               , "author"     .= ("Peikos"             :: Text)
                               , "color"      .= ("#FF0090"            :: Text)
                               , "head"       .= ("trans-rights-scarf" :: Text)
                               , "tail"       .= ("mystic-moon"        :: Text)
                               ]

-- Function move is where the magic happens!
-- Use [get| req.*] to retrieve information from request.
-- Docs: https://hackage.haskell.org/package/aeson-schemas
-- Structure: see Request.hs
--   or https://docs.battlesnake.com/references/api/sample-move-request
-- Result should be up, down, left or right (with or without shout).

move :: StdGen -> Object Request -> Object Move
move g req = head
           . mapMaybe (coordToDir . makeRelative (headCoordinate req) . fst)
           . sortOn snd
           . M.toList

             -- Occupied squares are always bad, so set cost to over 9000
             -- Future TODO: identify tail ends, which will be unoccupied next turn?
           . M.union (M.fromList [(coord, 9001) | coord <- enemyCoordinates req])

             -- Calculate distances to each food item
             -- and keep lowest distance for each square, use basemap if no food in range
           . foldr (M.unionWith min . distanceMap w h) (baseMap w h g)

           . foodCoordinates
           $ req

  where w = pred [get| req.board.width |]
        h = pred [get| req.board.height |]

baseMap :: Int -> Int -> StdGen -> Map Coordinate Int
baseMap w h g =
  M.fromList $
  zipWith (\noise (coord, val) -> (coord, val + noise)) (randomRs (0, 5) g) -- Add some random noise
    [((x, y), w + h + distanceFromCentre (w, h) (x, y)) | x <- [0..w] , y <- [0..h]]
      -- w + h assures food is always most important, as cost for non-food square > distance to food
      -- If snake has no more pressing concerns, try to stay near the centre to minimise expected
      -- travel times and maximise options.

      -- Random noise mainly added to counter stability of sorting algorithm, which will always
      -- favour lower coordinates when more than one share the same cost, leading to predictability.

distanceFromCentre :: (Int, Int) -> (Int, Int) -> Int
distanceFromCentre (w, h) (x, y) = abs (x - w `div` 2) + abs (y - h `div` 2)

distanceMap :: Int -> Int -> Coordinate -> Map Coordinate Int
distanceMap w h (fx, fy) = M.fromList [((x,y), abs (fx - x) + abs (fy - y))
                                      | x <- [0..w]
                                      , y <- [0..h]
                                      ]

makeRelative :: Coordinate -> Coordinate -> Coordinate
makeRelative (headX, headY) (x, y) = (x - headX, y - headY)

-- up, down, left and right yield JSON for given direction, plus optional shout.
up, down, left, right :: Maybe Text -> Object Move
up    = go "up"
down  = go "down"
left  = go "left"
right = go "right"

coordToDir :: Coordinate -> Maybe (Object Move)
coordToDir (1, 0) = Just (right $ Just "Yoinks!")
coordToDir (-1, 0) = Just (left $ Just "Wow!")
coordToDir (0, 1) = Just (up $ Just "Whee!")
coordToDir (0, -1) = Just (down $ Just "Ooh!")
coordToDir _ = Nothing

-- Retrieve head coordinates from request
headCoordinate :: Object Request -> Coordinate
headCoordinate req = convertCoord [get| req.you.head |]

-- Retrieve body coordinates from request
snakeCoordinates :: Object Request -> [Coordinate]
snakeCoordinates req = map convertCoord [get| req.you.body |]

-- Retrieve food coordinates from request
foodCoordinates :: Object Request -> [Coordinate]
foodCoordinates req = map convertCoord [get| req.board.food |]

-- Retrieve hazard coordinates from request
hazardCoordinates :: Object Request -> [Coordinate]
hazardCoordinates req = map convertCoord [get| req.board.hazards |]

-- Retrieve enemy coordinates from request (including your own)
-- This could be expanded to distinguish separate snakes, including head and hp,
--   which is left as an exercise.
enemyCoordinates :: Object Request -> [Coordinate]
enemyCoordinates req = [get| req.board.snakes |] >>= snakeCoords 
  where snakeCoords :: Object Snake -> [Coordinate]
        snakeCoords s = map convertCoord [get| s.body |]

-- Extracting integers from the JSON coordinate object
convertCoord :: Object Coord -> Coordinate
convertCoord coord = ([get| coord.x |], [get| coord.y |])

-- Formatting the JSON
go :: Text -> Maybe Text -> Object Move
go dir shout = coerceJson $ object [ "move" .= dir
                                   , "shout" .= shout
                                   ]

-- Logging based on request, just turn number for now
logRequest :: Object Request -> String
logRequest req = "Contemplating next move (turn " <> show [get| req.turn |] <> ") ... 🤔"
