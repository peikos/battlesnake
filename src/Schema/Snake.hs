module Schema.Snake where

import Data.Aeson.Schema (schema)

import Schema.Coord (Coord)

type Snake = [schema|
  { id: Text
  , name: Text
  , health: Int
  , body: List #Coord
  , head: #Coord
  , length: Int
  , latency: Text
  , shout: Maybe Text
  , squad: Maybe Text
  }
|]
