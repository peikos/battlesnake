module Schema.Info where

import Data.Aeson.Schema (schema)

type Info = [schema|
  { apiversion: Text
  , author: Text
  , color: Text
  , head: Text
  , tail: Text
  }
|]
