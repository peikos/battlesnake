module Schema.Request where

import Data.Aeson.Schema (schema)

import Schema.Coord (Coord)
import Schema.Snake (Snake)

type Request = [schema|
  { game:
    { id: Text
    , ruleset:
      { name: Text
      , version: Text
      }
    , timeout: Int
    }
  , turn: Int
  , board:
    { height: Int
    , width: Int
    , food: List #Coord
    , snakes: List #Snake
    , hazards: List #Coord
    }
  , you: #Snake
  }
|]
